const express = require('express');
const router = express.Router();
const productController = require('../controllers/productController');
const auth = require('../auth')



// Create Product - Post req (admin only)
router.post("/", auth.verify, (req, res) => {

	const isAdminData = auth.decode(req.headers.authorization).isAdmin;
	console.log(isAdminData)
	productController.createProduct(req.body, isAdminData).then(resultFromController => res.send(resultFromController))
});

// Retrieve all active products - Get all request
router.get("/all", (req,res) => {
	productController.getAllProducts().then(resultFromController => res.send(resultFromController))
});

// Retrieve a single product - Get request
router.get("/:productId", (req, res) => {
	productController.getProduct(req.params).then(resultFromController => res.send(resultFromController))
});

// Updating Product Details (admin only) - Put request
router.put("/:productId", auth.verify, (req, res) => {

	const data = auth.decode(req.headers.authorization)
	productController.updateProduct(req.params, req.body, data).then(resultFromController => res.send(resultFromController))
})

// Archiving Product (admin only) - Put request
router.put("/:productId/archive", auth.verify, (req, res) => {

	const data = {
		productId: req.params.productId,
		payload: auth.decode(req.headers.authorization).isAdmin
	}
	console.log(data)

	productController.archiveProduct(data).then(resultFromController => res.send(resultFromController))
});

/*// Retrieve All Orders (Admin only) - Get Request

router.get("/getAllOrders", auth.verify, (req, res) => {

	const data = {
		payload: auth.decode(req.headers.authorization).isAdmin
	}
	console.log(data)

	productController.getAllOrders(data).then(resultFromController => res.send(resultFromController))
});

*/


module.exports = router;