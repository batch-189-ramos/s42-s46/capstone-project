const express = require('express');
const router = express.Router();
const userController = require('../controllers/userController');
const auth = require('../auth')


// ROUTES

// User Registration
router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController))
});

// User Authentication - Post Login request
router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController))
});

// Set user as Admin - Put archive request

router.put("/:userId/setAsAdmin", auth.verify, (req, res) => {

	const data = {
		userId: req.params.userId,
		payload: auth.decode(req.headers.authorization).isAdmin
	}
	userController.userToAdmin(data).then(resultFromController => res.send(resultFromController))
});


// User will Place an order - Post request
router.post("/userOrders", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)

	let data = {
		userId: userData.id,
		isAdmin: userData.isAdmin,
		productId: req.body.productId,
		totalAmount: req.body.totalAmount,
		quantity: req.body.quantity
	}
	userController.userOrder(data).then(resultFromController => res.send(resultFromController))
});

// Retrieve Authenticated User's Order - Get request
router.get("/:userId/getOrders", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)

	let data = {
		userId: userData.id
	}

	userController.getUserOrder(req.params, data).then(resultFromController => res.send(resultFromController))
});

// Retrieve All Orders Route (Admin only) - Get request
router.get("/getAllOrders", auth.verify, (req, res) => {
	const data = {
		payload: auth.decode(req.headers.authorization).isAdmin
	}
	console.log(data)
	userController.getAllOrders(data).then(resultFromController => res.send(resultFromController))
})


module.exports = router;
