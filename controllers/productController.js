const Product = require('../models/Product')
const bcrypt = require('bcrypt');
const auth = require('../auth');

// Create products - to continue here
module.exports.createProduct = async (reqBody, isAdminData) => {
	if(isAdminData == true) {
		let newProduct = new Product({
			name: reqBody.name,
			description: reqBody.description,
			price: reqBody.price
		});
		return newProduct.save().then((product, error) => {
			if (error) {
				return false
			} else {
				return "Product was created"
			}
		})
	}
};


// Retrieve all active Products - Get route
module.exports.getAllProducts = () => {
	return Product.find({isActive: true}).then(result => {
		return result
	})
};

// Retrieve a single product route
module.exports.getProduct = (reqParams) => {
	return Product.findById(reqParams.productId).then(result => {
		return result
	})
};

// Updating Product details route
module.exports.updateProduct = async (reqParams, reqBody, data) => {
	if(data.isAdmin === true) {
		let updatedProduct = {
			name: reqBody.name,
			description: reqBody.description,
			price: reqBody.price
		}
		return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((product, error) => {
			if (error) {
				return false
			} else {
				return true
			}
		})
	} else {
		return "Not an Admin"
	}	
};

// Archiving Product route
module.exports.archiveProduct = async (data) => {
	return Product.findById(data.productId).then((result, error) => {
		if (data.payload === true) {
			result.isActive = false
			return result.save().then((archivedProduct, error) => {
				if(error) {
					return false
				} else {
					return true
				}
			})
		} else {
			return "Not an Admin"
		}
	})
}

/*// Retrieve All Orders route - to continue here

module.exports.getAllOrders = async (data) => {
	if (data.payload === true) {
		return Product.find({isActive: true}).then((result, error) => {
			if(error) {
				return false
			} else {
				return result
			}
		})
	} else {
		return "Not an admin"
	}
}*/