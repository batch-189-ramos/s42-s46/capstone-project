const User = require('../models/User');
const Product = require('../models/Product')
const bcrypt = require('bcrypt');
const auth = require('../auth');

// User Registration

module.exports.registerUser = (reqBody) => {
	let newUser = new User({
		email: reqBody.email,
		password: bcrypt.hashSync(reqBody.password, 10)
	})
	return newUser.save().then((user, error) => {
		if (error) {
			return false
		} else {
			return user
		}
	})
}

// User Auth - Post Login route
module.exports.loginUser = (reqBody) => {
	return User.findOne({email: reqBody.email}).then(result => {
		if (result == null) {
			return false
		} else {
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)
			if(isPasswordCorrect) {
				return {access: auth.createAccessToken(result)}
			} else {
				return false
			}
		}
	})
}

//Set user as Admin - Put route
module.exports.userToAdmin = (data) => {
	return User.findById(data.userId).then((result, error) => {

		if(data.payload) {
			result.isAdmin = true

			return result.save().then((updatedUser, error) => {
				if(error) {
					return false
				} else {
					return true
				}
			})
		} else {
			return "You are not an Admin"
		}
	})
}

// User Order route
module.exports.userOrder = async (data) => {
	if (data.isAdmin == true) {
		return "Admin not allowed to place order."
	} else {
		let isUserUpdated = await User.findById(data.userId).then(user => {

		let newTotalAmount = (data.totalAmount * data.quantity)

		user.orderCheckOut.push({productId: data.productId, quantity: data.quantity, totalAmount: newTotalAmount});

		return user.save().then((user, error) => {
			if(error) {
				return false
			} else {
				return true
			}
		})

	})

	let isProductUpdated = await Product.findById(data.productId).then(product => {

		product.orderCart.push({userId: data.userId});

		return product.save().then((product, error) => {
			if(error) {
				return false
			} else {
				return true
			}
		})

	})

	if (isUserUpdated && isProductUpdated) {
		return "Order was placed successfully!"
	} else {
		return false
	}
	}
}

// Retrieve User Order Route
module.exports.getUserOrder = (reqParams, data) => {
	
	if(data){
		return User.findById(reqParams.userId).then(result => {
			return result.orderCheckOut
		})
	}
}


// Retrieve All Orders route

module.exports.getAllOrders = async (data) => {
	if(data.payload === true) {
		return Product.find().then(productOrder => {
			let allOrders = []
			productOrder.forEach(productOrder => {
				allOrders.push({
					product: productOrder.name,
					orders: productOrder.orderCart
					
				})
			})
			return allOrders
		})
	} else {
		return "Not an Admin"
	}
}
