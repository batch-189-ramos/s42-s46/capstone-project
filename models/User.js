const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
	email: {
		type: String,
		required: [true, "Email is required"]
	},
	password: {
		type: String,
		required: [true, "Password is required"]
	},
	isAdmin: {
		type: Boolean,
		default: false
	},
	orderCheckOut: [
		{
			productId: {
				type: String,
				required: [true, "ProductID is required"]
			},
			quantity: {
				type: Number,
				required: [true, "No. of items per product required"]
			},
			totalAmount: {
				type: Number,
				required: [true, "Total Amount must be computed"]
			},
			purchasedOn: {
				type: Date,
				default: new Date()
			}
		}
	]
})

module.exports = mongoose.model("User", userSchema);